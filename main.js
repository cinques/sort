const MARGINS = {top: 0, bottom: 20, left: 10, right: 10 };
const WIDTH = 500 - MARGINS.right - MARGINS.left;
const HEIGHT = 500 - MARGINS.bottom - MARGINS.top;

var index = d3.range(20), // массив индексов (сортируются именно индексы)
    data = index.map(d3.random.normal(80, 20)); // массив данных для сортировки

// ось x
var x = d3.scale.linear()
    .domain([0, d3.max(data)])
    .range([0, WIDTH]);

// ось y
var y = d3.scale.ordinal()
    .domain(index)
    .rangeRoundBands([0, HEIGHT], .1);

// svg для отрисовки
var svg;

function draw() {
    svg = draw_chart();
    draw_bars(svg, x, y);
    draw_x_axis();
}

function draw_chart() {
    return d3.select("body")
        .append("svg")
            .attr("width", WIDTH + MARGINS.right + MARGINS.left)
            .attr("height", HEIGHT + MARGINS.bottom + MARGINS.top)
        .append("g")
            .attr("transform", "translate(" + MARGINS.left + "," + MARGINS.top + ")");
}

function draw_bars(svg, x, y) {
    var bar = svg.selectAll(".bar")
        .data(data)
      .enter().append("g")
        .attr("class", "bar")
        .attr("transform", (d, i) => "translate(0," + y(i) + ")");

    bar.append("rect")
        .attr("height", y.rangeBand())
        .attr("width", x);

    bar.append("text")
        .attr("text-anchor", "end")
        .attr("x", d => x(d) - 6)
        .attr("y", y.rangeBand() / 2)
        .attr("dy", ".35em")
        .text((d, i) => i);
}

function draw_x_axis() {
    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + HEIGHT + ")")
        .call(d3.svg.axis()
        .scale(x)
        .orient("bottom"));
}

draw();

const START_INTERVAL = 50;
var interval = START_INTERVAL;

document.getElementById('start').onclick = bubbleSort;

function bubbleSort() {
    var swapped;
    do {
        swapped = false;
        for (var i = 0; i < index.length - 1; i++) {
            if (data[index[i]] > data[index[i + 1]]) {
                [index[i], index[i + 1]] = [index[i + 1], index[i]]; // меняем местами
                swapped = true;
            }
            refreshChart(i);
        }
    } while (swapped);
}

function refreshChart(i) {
    // d3 отображает график в соответсвии с данными (массив индексов)
    y.domain(index);

    var bar = svg.selectAll(".bar");

    // Делаем все синим
    bar.select('rect')
        .transition()
        .delay(() => interval)
        .attr('style', 'fill: steelblue');

    // Выделяем зеленым обмениваемые значения
    bar.filter(x => x == data[index[i+1]] || x == data[index[i]]).select('rect')
        .transition()
        .delay(() => interval)
        .attr('style', 'fill: green');

    // Анимация перестановки
    bar.transition()
        .duration(350)
        .delay(() => interval += START_INTERVAL)
        .attr("transform", (d, i) => "translate(0," + y(i) + ")");
}